import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CellarController } from './CellarController';//Added
import { CellarService } from './cellar.service';


@Module({//added
  imports: [],
  controllers: [AppController,CellarController],//ok
  providers: [AppService,CellarService],
})
export class AppModule {}
