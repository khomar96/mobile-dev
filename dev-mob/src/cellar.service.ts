import { Injectable } from '@nestjs/common';
import { Cellar } from './cellar';//dossier/fichier
import { CellarDTO } from './CellarController';
import {Bottle} from './cellar';//Ok always put export before class in cellar.ts

//supprimer le fichier tslint.json

@Injectable()
export class CellarService {
    storage: Cellar[] = [];

    getAllCellars(): Cellar[] {
        return this.storage;
    }


    createCellar(cellarDto: CellarDTO): void {

        this.storage.push(new Cellar(cellarDto.name,0,"20"));
        




    }

     findById(id):Cellar{
      let Found=false;
      for( var i=0;i<this.storage.length;i++)
      {
           if(this.storage[i].Id===id)
           {
              Found=true;
              return this.storage[i];
           }
      }

    }

    getAllBottles(id): Bottle[] {
      return this.findById(id).getAllBottles();
  }

    createBottle(id,name,price){
           this.findById(id).AddBottle(name,price);

    }

}



  

/*export class cellarRepository{





}
*/




