import { Get,Param } from '@nestjs/common';
import { Cellar } from './cellar';




export class CellarRepository{
  constructor (private readonly cellarRepository:CellarRepository){}
  
} 



export interface CellarApi{
id:string;
name:string;
totalPrice:number;
numberOfBottles:number;
}


export function toCellarApi(cellar:Cellar):CellarApi{

  return {
    id:cellar.Id,
    name:cellar.Name,
    totalPrice:cellar.getTotalPrice().toDollar(),
    numberOfBottles:cellar.getAllBottles.length,


  };
}
