import { Controller, Get, Post ,Body,Param} from '@nestjs/common';

import { CellarService } from './cellar.service';
import { Cellar } from './cellar';
import { Bottle } from './cellar';
import { AppService } from './app.service';
import { tsParameterProperty } from '@babel/types';

@Controller( 'cellars')
export class CellarController {
  constructor(private readonly cellarService: CellarService) {}


  @Get()//
  getAll():Cellar[]{
    return this.cellarService.getAllCellars();//va dans app.service et appele get getHello
  }

  @Post()
  createCellar(@Body() cellarDto:CellarDTO){//data transfert object

  return this.cellarService.createCellar(cellarDto);

  }

  @Get(':id')
  findById(@Param('id') id):Cellar{
    return this.cellarService.findById(id);
  
  }

  @Get(':id/bottles')
  getAllBottles(@Param('id') id):Bottle[]{
    return this.cellarService.getAllBottles(id);
  
  }


  @Post(':id/bottles')
  createBottle(@Body() bottle:BottleDTO,@Param('id') id){//data transfert object

   this.cellarService.createBottle(id,bottle.name,bottle.price);//I had to create an interface BottleDTo
   //this.cellarService.createBottle(id,"Khatib",1000);

  }

}
/****************** */

 export interface CellarDTO{

  name:string;

}

export interface BottleDTO{

  name:string;
  price:number;

}