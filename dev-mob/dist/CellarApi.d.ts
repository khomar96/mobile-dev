import { Cellar } from './cellar';
export declare class CellarRepository {
    private readonly cellarRepository;
    constructor(cellarRepository: CellarRepository);
}
export interface CellarApi {
    id: string;
    name: string;
    totalPrice: number;
    numberOfBottles: number;
}
export declare function toCellarApi(cellar: Cellar): CellarApi;
