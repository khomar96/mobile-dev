"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Bottle {
    constructor(name, price) {
        this.Name = name;
        this.Price = price;
    }
}
exports.Bottle = Bottle;
class Cellar {
    constructor(name, size, id) {
        this.ArrayBottles = [];
        this.ArrayBottles = [];
        this.Name = name;
        this.size = 0;
        this.Id = id;
    }
    AddBottle(name, price) {
        const bottle = new Bottle(name, price);
        this.ArrayBottles.push(bottle);
    }
    getBottle(name) {
        let Found = false;
        for (var i = 0; i < this.ArrayBottles.length; i++) {
            if (this.ArrayBottles[i].Name === name) {
                Found = true;
                return this.ArrayBottles[i];
            }
        }
        if (Found === false) {
            console.log("Couldn't find the bottle \n");
        }
    }
    getAllBottles() {
        return this.ArrayBottles;
    }
    getTotalPrice() {
        let sum = 0;
        for (var i = 0; i < this.ArrayBottles.length; i++) {
            sum = sum + this.ArrayBottles[i].Price;
        }
        return {
            toDollar: () => {
                return sum * 0.8;
            },
            toEuro: () => {
                return sum;
            }
        };
    }
}
exports.Cellar = Cellar;
//# sourceMappingURL=cellar.js.map