import { Cellar } from './cellar';
import { CellarDTO } from './CellarController';
import { Bottle } from './cellar';
export declare class CellarService {
    storage: Cellar[];
    getAllCellars(): Cellar[];
    createCellar(cellarDto: CellarDTO): void;
    findById(id: any): Cellar;
    getAllBottles(id: any): Bottle[];
    createBottle(id: any, name: any, price: any): void;
}
