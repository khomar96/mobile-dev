"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CellarRepository {
    constructor(cellarRepository) {
        this.cellarRepository = cellarRepository;
    }
}
exports.CellarRepository = CellarRepository;
function toCellarApi(cellar) {
    return {
        id: cellar.Id,
        name: cellar.Name,
        totalPrice: cellar.getTotalPrice().toDollar(),
        numberOfBottles: cellar.getAllBottles.length,
    };
}
exports.toCellarApi = toCellarApi;
//# sourceMappingURL=CellarApi.js.map