"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cellar_service_1 = require("./cellar.service");
const cellar_1 = require("./cellar");
let CellarController = class CellarController {
    constructor(cellarService) {
        this.cellarService = cellarService;
    }
    getAll() {
        return this.cellarService.getAllCellars();
    }
    createCellar(cellarDto) {
        return this.cellarService.createCellar(cellarDto);
    }
    findById(id) {
        return this.cellarService.findById(id);
    }
    getAllBottles(id) {
        return this.cellarService.getAllBottles(id);
    }
    createBottle(bottle, id) {
        this.cellarService.createBottle(id, bottle.name, bottle.price);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Array)
], CellarController.prototype, "getAll", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], CellarController.prototype, "createCellar", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", cellar_1.Cellar)
], CellarController.prototype, "findById", null);
__decorate([
    common_1.Get(':id/bottles'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Array)
], CellarController.prototype, "getAllBottles", null);
__decorate([
    common_1.Post(':id/bottles'),
    __param(0, common_1.Body()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], CellarController.prototype, "createBottle", null);
CellarController = __decorate([
    common_1.Controller('cellars'),
    __metadata("design:paramtypes", [cellar_service_1.CellarService])
], CellarController);
exports.CellarController = CellarController;
//# sourceMappingURL=CellarController.js.map