export declare class Bottle {
    Name: string;
    Price: number;
    constructor(name: string, price: number);
}
export declare class Cellar {
    Name: string;
    ArrayBottles: Bottle[];
    size: number;
    Id: string;
    constructor(name: string, size: number, id: string);
    AddBottle(name: any, price: any): void;
    getBottle(name: any): Bottle;
    getAllBottles(): Bottle[];
    getTotalPrice(): TotalPrice;
}
interface TotalPrice {
    toEuro: () => number;
    toDollar: () => number;
}
export {};
