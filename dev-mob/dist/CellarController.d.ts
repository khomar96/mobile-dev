import { CellarService } from './cellar.service';
import { Cellar } from './cellar';
import { Bottle } from './cellar';
export declare class CellarController {
    private readonly cellarService;
    constructor(cellarService: CellarService);
    getAll(): Cellar[];
    createCellar(cellarDto: CellarDTO): void;
    findById(id: any): Cellar;
    getAllBottles(id: any): Bottle[];
    createBottle(bottle: BottleDTO, id: any): void;
}
export interface CellarDTO {
    name: string;
}
export interface BottleDTO {
    name: string;
    price: number;
}
