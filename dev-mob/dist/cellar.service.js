"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cellar_1 = require("./cellar");
let CellarService = class CellarService {
    constructor() {
        this.storage = [];
    }
    getAllCellars() {
        return this.storage;
    }
    createCellar(cellarDto) {
        this.storage.push(new cellar_1.Cellar(cellarDto.name, 0, "20"));
    }
    findById(id) {
        let Found = false;
        for (var i = 0; i < this.storage.length; i++) {
            if (this.storage[i].Id === id) {
                Found = true;
                return this.storage[i];
            }
        }
    }
    getAllBottles(id) {
        return this.findById(id).getAllBottles();
    }
    createBottle(id, name, price) {
        this.findById(id).AddBottle(name, price);
    }
};
CellarService = __decorate([
    common_1.Injectable()
], CellarService);
exports.CellarService = CellarService;
//# sourceMappingURL=cellar.service.js.map